use diesel::Insertable;
use crate::db::schema::task;

#[derive(Insertable)]
#[table_name = "task"]
pub struct NewTask<'a> {
    pub title: &'a str,
}

#[derive(Queryable, Serialize, Clone)]
pub struct Task {
    #[serde(skip)]
    pub id: i32,
    pub title: String,
    pub done: i32,
}