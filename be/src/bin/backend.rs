#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

use be::db::query_task;
use mytodo::Task;
use rocket_contrib::json::Json;
use rocket_contrib::databases::diesel;
use mytodo::JsonApiResponse;

#[database("testdb")]
struct DbConn(diesel::SqliteConnection);

#[get("/tasks")]
fn tasks_get(conn: DbConn) -> Json<JsonApiResponse> {
    let DbConn(ref connection) = conn;
    Json(JsonApiResponse {
        data: query_task(connection)
            .iter()
            .map(|db_task| db_task.into())
            .map(|task: Task| task.into())
            .collect()
    })
}

fn main() {
    rocket::ignite()
        .attach(DbConn::fairing())
        .mount("/", routes![tasks_get])
        .launch();
}

