use be::db::{create_task, establish_connection, query_task, set_task_done, delete_task};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
//#[structopt(name = "basic")]
enum Opt {
    #[structopt(name = "new")]
    New { title: String },
    #[structopt(name = "show")]
    Show,
    #[structopt(name = "done")]
    Done { id : i32 },
    #[structopt(name = "delete")]
    Delete { id: i32 }
}

fn main() {
    let opt = Opt::from_args();
    let connection = &establish_connection();
    match opt {
        Opt::New{title} => create_task(connection, &title),
        Opt::Show => query_task(connection).iter().for_each(|task|
            println!("{}) {} [{}]", task.id, task.title, if task.done != 0 {"v"} else {" "})
        ),
        Opt::Done{id} => set_task_done(connection, id),
        Opt::Delete{id} => delete_task(connection, id)
    }
}

