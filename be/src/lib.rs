#[macro_use]
extern crate diesel;
#[macro_use]
extern crate serde;

pub mod db {

    use diesel::{prelude::*, sqlite::SqliteConnection};
    use mytodo;

    pub mod models;
    pub mod schema;

    impl From<&models::Task> for mytodo::Task {
        fn from(db_task: &models::Task) -> Self {
            mytodo::Task {
                id: db_task.id,
                title: db_task.title.clone()
            }
        }
    }

    pub fn establish_connection() -> SqliteConnection {
        let db = "./testdb.sqlite3";
        SqliteConnection::establish(db)
            .unwrap_or_else(|_| panic!("Error connecting to {}", db))
    }

    pub fn create_task(connection: &SqliteConnection, title: &str) {
        let task = models::NewTask { title };

        diesel::insert_into(schema::task::table)
            .values(&task)
            .execute(connection)
            .expect("Error inserting new task");
    }

    pub fn query_task(connection: &SqliteConnection) -> Vec<models::Task> {
        schema::task::table
            .load::<models::Task>(&*connection)
            .expect("Error loading tasks")
    }

    pub fn set_task_done(connection: &SqliteConnection, target_id: i32) {
        use schema::task::dsl::*;

        let target = task.filter(id.eq(target_id));

        diesel::update(target)
            .set(done.eq(1))
            .execute(connection)
            .expect("Error updating task");
    }

    pub fn delete_task(connection: &SqliteConnection, target_id: i32) {
        use schema::task::dsl::*;

        diesel::delete(task.filter(id.eq(target_id)))
            .execute(connection)
            .expect("Error deleting task");
    }
}