#[macro_use]
extern crate serde;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Task {
    pub id: i32,
    pub title: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Resource<T> {
    id: String,
    #[serde(rename = "type")]
    typ: String,
    attributes: T
}

impl From<Task> for Resource<Task> {
    fn from(task: Task) -> Self {
        Resource {
            id: task.id.to_string(),
            typ: "task".into(),
            attributes: task.clone()
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct JsonApiResponse {
    pub data: Vec<Resource<Task>>,
}
